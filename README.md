# P18 - Getting Started with our DIS-Project

Team members: 

- Rathnakar Reddy Ettedi 
- Anirudh Gannarapu

Developed as part of the profession-based learning cirriculum at:

44-564 Design of Data Intensive Systems, 
Northwest Missouri State University, 
Maryville, MO.

# Contributors

Client

- Denise Case, PhD, PE,  Assistant Professor,   Assistant Coordinator, Masters of Applied Computer Science Program, 
  School of Computer Science and Information Systems, 
  Colden Hall 2280, 
  Northwest Missouri State University, 
  800 University Drive, Maryville, MO 64468, 
  dcase@nwmissouri.edu     
  https://www.linkedin.com/in/denisecase

Faculty

- Dr.Denise Case

Assistants

- Sai Sri Ravali Chinthareddy, Course Assistant

Developers, Designers, and Software Engineers

- Rathnakar Reddy Ettedi 
- Anirudh Gannarapu

# Prerequisites

Following must be installed to run this application:

1. Python (Version 2.6.6+)
2. Notepad++ (recommended)

# Get started

Clone this repo to your local machine. 

If Windows, 

- Right-click on C:\44564.
- git clone https://RathnakarReddyE@bitbucket.org/RathnakarReddyE/dis-project-p18.git

## Overview

#Glimpse about our dataset:

- Natural gas consumption for residential purpose is our dataset. We feel our dataset is a good data source, since we got the dataset from official website of U.S Energy Information Administration department. Link to our dataset: https://www.eia.gov/opendata/qb.php?category=457053. We have considered the dataset of around 25 states of United States as of now. We feel our dataset constitutes of regular life details which is easy to understand/process/debug.

# Short introduction to our project:

- Our project is mapreduce program that finds the maximum, minimum, total & average natural gas consumption for residential purposes for each state over a period of Jan-1989 through Dec-2016.

# Process

Initially, our input dataset is feed to the mapper program to retrieve required fields of dataset. Output of mapper program is sorted & shuffled before passing it to the reducer program.
In reducer program, we apply various aggregate functions to generate maximum, minimum, total & average volumes of natural gas consumed for each state.

# Initialize (do these just once when setting up the project)

Clone a copy of repository to your c:\44564 folder.

Open a command window as Administrator in this project folder (e.g. C:\44564\P18).

Follow the below commands:

```
> python mapper.py
```
![Mapper_Output.PNG](https://bitbucket.org/repo/x8AayMA/images/926946787-Mapper_Output.PNG)

The output of mapper function is generated in a file: mapper_output.txt.
This file is used as an input for reducer program.

```
> python reducer.py
```
![Reducer_Output.PNG](https://bitbucket.org/repo/x8AayMA/images/3466389435-Reducer_Output.PNG)

The output of reducer function is generated in a file: reducer_output.txt. It contains the details like maximum, minimum, total & average consumption volumes for each state.

## Graphical Representation of results:

![Graphical_Representation_Latest.PNG](https://bitbucket.org/repo/x8AayMA/images/699768030-Graphical_Representation_Latest.PNG)

## Resources and References


How to add "Open Command Window Here as Administrator" to context menu:
https://www.sevenforums.com/tutorials/47415-open-command-window-here-administrator.html